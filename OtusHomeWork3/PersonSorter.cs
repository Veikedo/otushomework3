﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OtusHomeWork3
{
    public class PersonSorter : ISorter<Person>
    {
        public IEnumerable<Person> Sort<Person>(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(x => x);
        }
    }
}
