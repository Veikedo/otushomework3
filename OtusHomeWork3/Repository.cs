﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OtusHomeWork3
{
    public class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }

    // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }

    public interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
    }

    public class JsonRepository : IRepository<Account>
    {
        string path = "accountrepository.ndjson";
        public void Add(Account item)
        {      
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(JsonSerializer.Serialize(item));
            }
        }

        public IEnumerable<Account> GetAll()
        {
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    var accaunt = JsonSerializer.Deserialize<Account>(sr.ReadLine());
                    if(accaunt != null)
                        yield return accaunt;
                }
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }
    }

    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> _repository;
        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account.FirstName))
                Console.WriteLine("FirstName должно быть заполнено");
            if (string.IsNullOrEmpty(account.LastName))
                Console.WriteLine("LastName должно быть заполнено");

            DateTime today = DateTime.Today;    
            TimeSpan age = today - account.BirthDate;     
            double ageInYears = age.TotalDays / 365;

            if (ageInYears > 18)
                _repository.Add(account);
            else
                Console.WriteLine("Возраст меньше 18 лет");
        }
    }
}
