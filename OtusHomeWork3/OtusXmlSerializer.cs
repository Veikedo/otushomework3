﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace OtusHomeWork3
{
    public interface ISerializer<T>
    {
        string Serialize<T1>(T1 item);
        T1 Deserialize<T1>(Stream stream);
    }

    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings XmlWriterSettings;

        static OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
        }        

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }

        public T1 Deserialize<T1>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
               .UseAutoFormatting()
               .UseOptimizedNamespaces()
               .EnableImplicitTyping(typeof(T1))
               .Create();

            return serializer.Deserialize<T1>(stream);
        }

    }
}
