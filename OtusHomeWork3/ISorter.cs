﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusHomeWork3
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
    }
}
